//
//  ReviewViewController.swift
//  Instant Movie Review
//
//  Created by Raees on 11/07/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import AVFoundation
import UIKit

class ReviewViewController: UIViewController {
    
    var captureSession: AVCaptureSession?
    var isbn : String?
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ReviewViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        let api = UPCDatabaseAPI(isbn: self.isbn)
        
        if let title = api.fetchTitle() {
            label.text = title
        } else {
            label.text = "NULL"
        }
    }
    
    func back(sender: UIBarButtonItem) {
        captureSession?.startRunning()
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
