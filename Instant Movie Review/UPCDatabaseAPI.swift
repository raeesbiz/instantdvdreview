//
//  UPCDatabaseAPI
//  Instant Movie Review
//
//  Created by Raees on 11/07/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

class UPCDatabaseAPI {
    
    var isbn : String?
    static let API_KEY = "065bf340b80bdbcad4d75ff0ae77b3ac"
    
    init(isbn: String?) {
        self.isbn = isbn
    }
    
    func fetchTitle() -> String? {
        let urlStr = "http://api.upcdatabase.org/xml/" + UPCDatabaseAPI.API_KEY + "/" + isbn!
        guard let url = URL(string: urlStr) else {
            print("Error: \(urlStr) doesn't seem to be a valid URL")
            return nil
        }
        
        do {
            let xml = try String(contentsOf: url, encoding: .ascii)
            let descs = compile(for : "<description>(.*?)</description>", in: xml)
            let desc = descs?[0].replacingOccurrences(of: "<description>", with: "").replacingOccurrences(of: "</description>", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
            let itemnames = compile(for : "<itemname>(.*?)</itemname>", in: xml)
            let itemname = itemnames?[0].replacingOccurrences(of: "<itemname>", with: "")
            .replacingOccurrences(of: "</itemname>", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
            if desc == "" {
                return itemname
            } else {
                return desc
            }
        } catch let error {
            print("Error: \(error)")
        }
        
     return nil
    }
    
    private func compile(for regex: String, in text: String) -> [String]? {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let str = text as NSString
            let matched = regex.matches(in: text, range: NSRange(location: 0, length: str.length))
            return matched.map { str.substring(with: $0.range)}
        } catch let error {
            print("Incorrect expression: \(error.localizedDescription)")
            return nil
        }
    }}
