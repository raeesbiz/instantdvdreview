//
//  Movie.swift
//  Instant Movie Review
//
//  Created by Raees on 11/07/2017.
//  Copyright © 2017 Raees Apps. All rights reserved.
//

import Foundation

struct Movie {
    var title: String
    var year: Int
    var releaseYear: String
    var runtime: String
    var actors: [String]
    var plot: String
    var posterURL: String
    var language: String
    var imdbRating: Double
    var imdbVoters: Int
    var imdbID : String
    
}
